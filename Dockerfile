# This is the Dockerfile for the kongr45gpen/acubesat-ci:latest
# Docker image.

FROM alpine:3.12 AS hil-buildenv

WORKDIR "/root/"
RUN apk add --no-cache libffi-dev curl rust python3 cargo git py3-setuptools python3-dev py3-pip py3-wheel

WORKDIR "/root/"
RUN curl -O https://keilpack.azureedge.net/pack/Keil.STM32L4xx_DFP.2.4.0.pack
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3
RUN git clone https://gitlab.com/acubesat/obc/hil-tests.git
ENV PATH "$PATH:/root/.poetry/bin/"

WORKDIR "/root/hil-tests/"
RUN poetry install
RUN mv $(poetry env info -p) ~/virtualenv


FROM alpine:3.12 AS hil-environment

WORKDIR "/root/"
RUN echo "@testing http://nl.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
    apk add --no-cache python3 git libusb cmake newlib-arm-none-eabi@testing gcc-arm-none-eabi@testing  
COPY --from=hil-buildenv /root/virtualenv ./virtualenv
COPY --from=hil-buildenv /root/Keil*.pack .

ENV VIRTUAL_ENV=/root/virtualenv
ENV PATH="$PATH:$VIRTUAL_ENV/bin"

ENTRYPOINT "/bin/sh"
